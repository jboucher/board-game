title: NPC Spawning and Action Phase
description: NPC Spawning and Movement mechanics
authors: Justin Boucher

# NPC Movement and Spawning

!!! warning "Sometimes Everything Changes"
    Different scenarios may have their own NPC movement and spawning rules. Consult the corresponding scenario guide for details.

## NPC Movement

NPC Movement is not performed by the player. Instead it is performed the other players for every NPC on the board. This means that each once a player has finished their movement and action phase for all of their player tokens, the players waiting on their turn team up to move the NPC according to the corresponding movement type listed in the [Movement Types](#movement-types) section, even if that movement is toward their own player tokens.

### Player Token Collision

NPCs will collide with player tokens on the board during NPC movement phases. The action performed by the NPC depends of the following circumstances:

* If a NPC is within 2 grid squares of the active player (player who just completed their movement and action phase), an attack roll will be performed following the standard attack procedures list in the [General Actions for All Tokens](player_movement.md#general-actions-for-all-player-tokens) Section of the player movement and action document. The active player may finish closing out their turn after the NPC attack phase has been settled. See [Closing Out Your Turn] for guidance on the remaining steps to complete your turn.

* If a NPC is within 2 grid squares of an inactive player, the inactive player must perform the attack action roll. If the inactive player takes damage during the attack they will immediately apply that damage to their player token.

### Movement Types

|Type| Description|
|:---|:-----------|
|**Searching**| In this movement type, each NPC moves 2 square in the direction of the nearest player token (thats an approximate). If their are multiple player tokens within the same approximate distance, the NPC moves in whichever direction they are currently facing. |
|**Hunting**|Once a player ends his moves 10 squares away or less, NPCs start moving with a speed of 4 grid squares, and they always take the same route the player's token took during their turn (they can smell the player's token).

## NPC Spawning

NPC Spawning is dependent on the individual scenario agreed upon by the players. If no spawning mechanic is listed in the scenario documentation, then the default spawning scenario is used. For each NPC that has been defeated during the player's turn, one NPC of a random type will be placed on the board by a portal entrance and commence their movement phase.