title: Card Catalog
description: List of cards for each type
authors: Justin Boucher

# Card Catalog

This document contains a list of cards that come with the game, and have been broken up into the appropriate decks.

## Lore Cards

The Lore card stack consists of 30 cards, unless you lost or ate one. If you run out during the game, just reshuffle from the discard pile.

| Card Title | Description | Count |
|:-----------|:------------|:------|
| Was that you? | Nope, that's just a cloud of poisonous gas. Player tokens within a 4 grid square diameter take 1 health damage at the end of their active turn until they have been to a Healer (including inactive player's tokens). Keep this card until all tokens have been cured, or have been downed. Discard this card once depleted. | 2|
| How did they fit that in there? | Replace resource token with Level 2 NPC, and get ready to fight. If NPC is not downed during attack, they resume normal NPC movement phases. Place this card in the discard pile. | 4 |
| It's a trap! | This resource has been booby-trapped. Player tokens within 1 grid square roll 1d4 for damage and remove it from armor durability. If token has no armor remove the damage from health. Place this card in the discard pile | 4|
|It's my birthday!| Player selects one Armor or one Weapon card from the top of the deck and places it directly within their stores. Discard this card after use. | 4|
| False bottom? | You found a false bottom in this chest! Role the resource die again and add two additional units of the received resource (queue carry capacity not effected, because it's magic!). Re-roll if Lore card is rolled again. Discard this card after use. | 4|
| Tribal Rage | Every attack checking roll for any of your player tokens has a +1 modifier, because you deserve this. This card is kept throughout the game and can be stacked.| 2|
| This space for rent | This card has been left intentionally blank. (Check your Scenario for special Lore modifiers and follow directions. If no special modifiers are listed, discard and redraw a Lore card.) | 2|

## Weapons Cards

| Card Title | Description | Count |
|:-----------|:------------|:------|

## Armor Cards

| Card Title | Description | Count |
|:-----------|:------------|:------|