title: Home
description: Introduction
authors: Justin Boucher

# Project Redhand Design Document


## Introduction
Project Redhand is the psuedonym for a modular scenario based resource management tabletop board game.

## Gameplay Phases
1. [Player Token Movement and Action Phase](player_movement.md)
2. [NPC Movement and Spawning Phase](npc_movement.md)
3. Turn Complete