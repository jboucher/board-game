title: Initial Game Setup
description: Initial Board Setup
authors: Justin Boucher

# Game Setup

`0. Select Scenario`

:   Players need to identify the scenario they will be playing for this game. Each scenario may have different setup requirements. See the corresponding Scenario document to determine if the steps below need to be adjusted to meet the scenario.

`1. Layout Game Panels`

:   Layout panels in desired configuration. See [Game Panel Setup Configuration](#1-game-panel-setup-configuration) for more information.

`2. Place Resource Tokens`

:   Place resource token on each resource location. Available resource locations are marked on the board by a circle with dashed lines. See [Resource Placement](#2-resource-placement).

`3. Shuffle Cards`

:   Shuffle and place cards facedown into their individual stacks according to the icon on the back of the card. Then place to the side of the board. Card specific information is listed in the [Card Catalog](cards.md).

`4. Select Base Camp`

:   Each player will select their starting base camp. Base camps are denoted by a campfire icon on the game board. [Base Camp Selection](#4-selecting-a-base-camp) information.

`5. Place NPCs on Board`

:   Each player will place 2-3 NPC tokens randomly on the board depending on the scenario they will be playing. See [NPC Placement](#5-placing-npcs) for details.

`6. Roll Starting Resource`

:   Each player has a starting resource. This resource is immediately added to their stores and is usable from the beginning of the game. See [Starting Resource](#6-starting-resources) to determine number of resources to add to stores.


## 1. Game Panel Setup Configuration

This game consists of up to 8 panels placed in any configuration, and a portal panel that must be placed as near the center of the configuration as possible for NPC spawning. Grid squares between panels must be in alignment, but top and bottom of panels do not have to align.

!!! tip
    For shorter games, or reduced amount of players, not all panels must be used, but the portal panel must be used as it is the spawning point for NPC tokens. Denoted in purple below.

### Panel Layout Examples

<figure markdown>
  ![Board Layout Examples](images/gameboard_layout.png){ width="300" }
  <figcaption>Example board layouts</figcaption>
</figure>

#### Recommended Number Panels for Games

| # of Players | # of Panels |
|:-------|:---------|
|2| 5 + Portal |
|3| 6 + Portal |
|4+| 8 + Portal |

### 2. Resource Placement

![campfire](images/resource_icon_sm.png){ align=left } All resource locations are marked with a dashed circle containing the resource icon. Resource tokens should be placed with the front of the token facing up. Turning the token over will show that the resource has been depleted. Depending on the agreed upon scenario some resource slots may not be used, or resource depletion will not be required.

#### Resource Token Front and Back

|Front|Back|
|:----|:---|
|<img src="../images/coins.png"/>|<img src="../images/removed.png"/>

### 3. Shuffle Cards
I think you got this.

### 4. Selecting a Base Camp

![campfire](images/campfire_sm.png){ align=left }Each game panel, except the Portal panel, contains one Base Camp denoted by a campfire icon. Some Base Camps are larger and contain multiple entrances/exits, and some contain only one entrance/exit. Selecting the right Base Camp is important as a player may not move their Base Camp during the game. Selecting a Base Camp with multiple ingress/egress points will make it easier for the scavenger tokens to return resources to the Base Camp stores from multiple angles, but also provide multiple points for attacks and raiding.

### 5. Placing NPCs

NPCs will be placed on the game board in random locations according to the scenario. If no NPC placement criteria is listed in the scenario, the default behavior is for every to to place 2-3 NPCs on the board at their discretion. However, even NPCs think camping is rude. NPCs may not be placed within 20 grid squares of an active player's Base Camp. NPC initial spawn count guidance is below:

| # Players| # NPC Spawned|
|:---------|:-------------|
|2| 3|
|3| 3|
|4+| 2|

### 6. Starting Resources

!!! Danger
    The faces on the die for initial game setup do not represent the same resource as they do during gameplay. See the [Resource Guide](resources.md) for more information.

| Die Face | Type | # Resources | Description |
|:---------|:-----|:------------|:------------|
|<img src="../images/shield.png" width=25/>|Metal|15|Building Material|
|<img src="../images/wood.png" width=25/>|Wood|15|Building Material|
|<img src="../images/leather.png" width=25/>|Armor|5|Random cards from top of armor deck|
|<img src="../images/grain.png" width=25/>|Grain|30|Sustains Player Tokens|
|<img src="../images/ammo.png" width=25/>|Weapons|5|Random cards from top of weapon deck|
|<img src="../images/book.png" width=25/>|Lore|5|Random cards from top of weapon deck|

#### Additional Starting Resources

Each Player will receive additional starting resources at lower quantities to begin their journey. A player ***may not*** add the quantities from the table below to their stores below for the starting resource they rolled. For instance, if a player rolled metal for their starting resource, they may not add the additional metal resources listed in the table below to their stores, as they already 15 metal resources to begin with. Weapon and Lore cards are dealt from the top of the corresponding deck to the player. 

!!! info "Lore Card Notes"
    Lore cards may be used at the beginning of the game unless otherwise stated on the card. Lore cards are the only resource that go immediately to the players store and each card contains directions for usage. More information about Lore Cards can be found in the [Card Catalog](cards.md).

| Type | # Resources |
|:-----|:------------|
|Metal| 5 |
|Wood| 5 |
|Leather| 5 |
|Grain| 15 |
|Weapons|3|
|Ranged Ammo|9|
|Lore|1|