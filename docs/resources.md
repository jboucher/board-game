title: Resource Guide
description: Resource Guide
authors: Justin Boucher

# Resources Guide

## Types of Resources to Gather

| Die Face | Type | Description |
|:---------|:-----|:------------|
|<img src="../images/shield.png" width=25/>|[Metal](#metal)| Crafting Resource |
|<img src="../images/wood.png" width=25/>|[Wood](#wood)| Crafting Resource|
|<img src="../images/leather.png" width=25/>|[Leather](#leather)| Crafting Resource|
|<img src="../images/grain.png" width=25/>|[Grain](#grain)|Food,Water, and Health|
|<img src="../images/ammo.png" width=25/>|[Ranged Ammo](#ranged-ammo)|Pre-crafted ammo for ranged weapons|
|<img src="../images/book.png" width=25/>|[Lore](#lore)|Old-World knowledge. Modifies player tokens in some way. |

### Metal

!!! tip inline end "It's soo heavy!"
    Materials like metal have weight. For instance, Scavengers cannot wear Heavy armor and still carry all resources in their queue. Make sure you are only crating what you need to survive, since metal takes more time to work then wood.

Metal is a critical material for building strong defenses. Metal is used in all craftable materials, and a player will craft a lot of materials during the game in order to keep their tokens alive. Some examples of craftables with metal:

| Type | Purpose |
|:-----|:--------|
| **Armor** | All Medium and Heavy armor types require metal in order to craft. This is pretty much standard in any game that has armor.
| **Weapons** | Most weapon crafting will require metal in some quantity. Sharp pointy tips are needed for the slaying. |
|**Carts**| Carts mostly need wood to craft, but they also need nails...so you need some metal.|
|**Defenses**| Heavier defense types provide better durability then wood, but require sturdier materials for maximum effect.|

### Wood

!!! tip inline begin "Carts save backs and lives!"
    Carts may seem like a waste of resources at the beginning of the game, but they do increase the ability for your scavengers to carry more items in their queue. Less back and forth between resource sites in NPC and raider infested territory might just save their lives.

Wood is another critical material for building not only weapons, but materials that can greatly assist your survival strategy. Although larger crafting projects mostly require metal, wood is a resource that will constantly be used, and it's work time is significantly less then metal. Some examples of craftables with wood:

| Type | Purpose |
|:-----|:--------|
| **Ranged Ammo** | Arrows, bolts, and spears are all mostly made of wood. Attacking from a distance is always better then punching your way through. |
| **Weapons** | Like ranged ammo, some weapons are mostly wood in nature. Still better then punchy-punchy time. |
|**Carts**| Carts are mostly wood to craft, but does take a significant amount of time to complete.|
|**Defenses**| Lighter defense types provide less durability then metal, but take a lot less time then metal to build and repair.|


### Leather

!!! tip inline end "Just like the old days!"
    Defeated NPCs can be scavenged for hide, which can be crafted into leather by the Tinkerer. If you need more leather, maybe try a little NPC smashing.

Leather is not only a scavenged material, but is also craftable by the Tinkerer. Almost every craftable material needs small amounts of leather to complete, whether that is for strapping or coverings. Leather is a quick resource to craft with, but needs to either be scavenged whole or turned into refined leather via hide resources. Some examples of craftables with leather:

| Type | Purpose |
|:-----|:--------|
| **Armor** | Light and Medium armor are mostly made from hardened leather. Heavy armor needs to be secured via straps; you know...like leather straps. |
| **Sachels/Quivers** | Who doesn't like a new fanny-pack filled with crossbow bolts? Sachels/quivers can increase carrying capacity for ranged ammo. |
|**Backpacks**| Increasing your carrying capacity for scavenging while remaining light on your feet. Not quite as much as a cart, but keeps your movement speed intact. |
|**Defenses**| Most defenses require small amounts of leather. Whether thats for securing straps, or even a tarp to cover a pit of spikes. |

### Grain

!!! tip inline begin "All pigs get slaughtered!"
    Food is not bountiful in the world for most players. Use it wisely as it depletes quicker then you think.

We all have to eat and drink to survive, and so do your tokens. At the end of each turn your food stores are depleted by one unit. Once you run out of food, it affects each tokens health by one for every round without food resources in your stores. In addition, healers use food based resources to creating their healing potions.

### Ranged Ammo

!!! tip inline end "Blowing kisses"
    Stock your scavengers with as many ranged weapons as possible since they aren't as well durable as other tokens. Just remember that they require ammo to work.

Ranged ammo can be scavenged already pre-made, or be crafted as well. Although your tokens can only carry a specific amount of ammo, it can make the difference between winning or losing a conflict. Once you are out of ammo, it's time to get up close and personal.

### Lore

!!! danger inline "It's a crapshoot"
    Not all Lore cards are beneficial. A lot of them may injure the player's tokens, such as booby traps. However, they can also provide many benefits by adding fully crafted items to your store, or even effecting other players or NPCs.

Old world lore isn't really a resource to collect as much as a boon or deficit to your player tokens. These cards will likely completely change your gameplay strategy. Every card has something that will modify your player tokens. Some cards have temporary effects and some have entire game lasting effects. Lore cards are utilized right away, unless otherwise stated on the card. When a player rolls <img src="../images/book.png" width=25/> on the resource die, they will draw one card from the top of the Lore deck and follow the instructions on the card.
