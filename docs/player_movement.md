title: Player Movement and Action Phase
description: Player movement and action mechanics
authors: Justin Boucher

## Turn Based Movement and Action

All turns consist of individual movements and actions from each of your player tokens, and each token will be able to perform one movement, and one action per turn. A player may choose to perform an action before a movement, or reduce their movement; but cannot split their movement into sections. Player will roll all ***1d6*** for each of the available tokens (player tokens that can perform a movement this turn) at once and determine which die will represent each token during the turn.

### Movement and Action Examples 

1. Player rolls a 4 for a token, and moves the token 4 spaces. No viable action is around the player token. No action phase can be taken and the turn for that token is over.

2. A player rolls a 6, but the resource is 4 grid squares away. Player moves 4 grid squares and performs the resource collection action. The tokens ability to move again this turn is depleted.

3. A player downed an NPC at the end of the last turn, they can choose to scavenge the downed NPC, and then perform their movement.

4. A player chooses to keep their Tinkerer at basecamp, and continues reducing the Tinkerer's work timer. No movement die is cast for this player token.

## Basic Turn State Machine Diagram

``` mermaid
stateDiagram-v2
    [*] --> MovePlayerToken: Roll 1d6 for movement per player token
    MovePlayerToken --> CheckRange: Check if in range to action
    CheckRange --> [*]: False
    CheckRange --> PerformAction: True
    PerformAction --> IsSuccess
    note left of IsSuccess
        See corresponding Actions table for reference.
    end note
    IsSuccess --> [*]: False
    IsSuccess --> CollectReward: True
    CollectReward --> [*]
```

## General Actions for All Player Tokens

|Action|Success Criteria|Reward|
| :------ | :---------------- | :------ |
|**Attack Player**|Each player in attacking or being attacked rolls ***1d10***. Highest roll is successful. See info on Ranged Weapons | Player with highest role will roll ***1d6*** (***1d4 for Scavenger***) for damage to affected player|
|**Attack NPC**| Player will roll **Black** ***1d10*** to represent NPC and **Red** ***1d10*** to represent player. Highest role is successful | Player roles ***1d6*** (***1d6 for Scavenger***) to determine damage to affected player or NPC|

## Token Specific Actions

### Scavenger Actions

!!! note
    Scavengers are encumbered by adding resources to their resource queue. They must return the resource to their basecamp to remove resources from their queue into their basecamp's stores. While encombered they move at half speed. Scavengers may use a cart to increase their carrying capacity to **4 queue slots** but the cart movement speed is reduced by half. The cart must be taken back to their basecamp to move the resources from the queue to their stores.

|Action|Success Criteria|Reward|
| :------ | :---------------- | :------ |
|**Collect Resource Token**|Scavenger token is within 1 grid square of resource token. | Player rolls ***1d4+<number of scavengers within 2 grid squares of resource\>*** for number of resources to add to their resource queue and will roll the resource die to determine what type of resource they received. See resource mechanics for more information. | 
|**Collect Resource From Player Token**| Scavenger token is within 1 grid square of downed player. | Player will obtain all resources in the downed player's queue. |
|**Collect Resource From NPC Token**|Scavenger token is within 1 grid square of downed NPC. | Player rolls ***1d4*** for number of **hide** resources to add to their resource queue. | 
|**Raid**| Scavengers are within another players unprotected base camp or within 1 grid square of another players unprotected cart. | <ul><li>If a Scavenger is in a basecamp they will roll ***1d4+<number of scavengers within 2 grid squares of basecamp\>*** but can choose which resource to remove from the other players stores.</li><li>If a Scavenger is within one grid square of another players cart they will roll ***1d4+<number of scavengers within 2 grid squares of cart\>*** but can choose which resource to remove from the carts resource queue.</li></ul> |
|**Trade**| Scavengers can trade resources with another player by taking resources from their queue to another player's basecamp to add to their store, or receive resources from another player's basecamp and add it to their queue if they are not already encumbered by travelling to their own basecamp.| Scavneger token removes resource from queue and the resource is added to the other player's basecamp. If they are receiving resources they can add the resource to their queue. |

### Healer Actions

|Action|Success Criteria|Reward|
| :------ | :---------------- | :------ |
|**Heal Downed Player Token**|Healer token is within 1 grid square of downed player token. | Player restores downed player token to a health of 1 at the end of the turn. Additional healing can be performed on the next turn. Healing actions reduce one grain unit from stores immediately. ***NOTE: Healers can restore any player or NPC tokens*** |
|**Heal Player Token**|Healer token is within 1 grid square of a player token. | Player will roll ***1d4*** to determine amount of health added to the player token but player token cannot make a movement or action during this turn. Healing actions reduce one grain unit from stores immediately.***NOTE: Player can heal their Healer token the same way, but cannot perform a movement during this turn.*** |

### Tinkerer Actions

!!! note
    Tinkerers cannot split their actions between projects. Once they start crafting, they must complete the work timer for the current item before moving to another item.

|Action|Success Criteria|Reward|
| :------ | :---------------- | :------ |
|**Craft**|Tinkerer token has the available resources at basecamp that are noted on the craftable type table. | Player rolls ***1d6*** to determine if they can reduce the work time for that round but cannot perform a movement while crafting.<ul><li>Player rolls ***1-3*** they remove one unit from the work timer immediately for the craftable item.</li><li>Player rolls ***4-6*** they remove two units from the work timer immediately for the craftable item.</li></ul>| 
|**Repair**|Tinkerer token has the available resources at basecamp that are noted on the armor type table. | Player rolls ***1d6*** to determine if they can reduce the work time for that round but cannot perform a movement while repairing.<ul><li>Player rolls ***1-3*** they remove one unit from the work timer for the craftable item.</li><li>Player rolls ***4-6*** they remove two units from the work timer for the craftable item.</li></ul>| 

#### Types of Craftables

!!! note
    Tinkerer's completed craftables are moved to the basecamp stores. Player's tokens must be at basecamp to swap their current items for newly crafted ones.

|Type|Purpose|Times Repairable|Additonal Information|
|:---|:------|:----------|:-------------------|
|**Armor**| Adds additonal health to player. This additional health is depleted prior to affecting the health of a player token, and is listed on the armors durability rating | 1 time at half durability | See Armor Types table for specific information|
|**Defenses**| Adds defenses around basecamp or resource site. Defenses prevent other players or NPC from crossing the protected boundary until the durability has been depleted | Unlimited | See Defense Types table for specific information |
|**Ranged Ammo**| Ammo for ranged weapons | N/A | Player rolls **1d6** to determine how much ranged ammo is produced during turn. Ranged ammo requires 1 metal, 1 wood, and one hide resource to craft per unit. ***Example: Player rolls a 4 they need 4 Metal, 4 Wood, and 4 Hides to produce the total amount.*** |
|**Weapons**| Replaces current weapon of specified token. Each token can only carry one weapon at a time | N/A | Player will draw a weapon card from the weapon deck, and perform the appropriate crafting steps for the listed weapon |
|**Carts**|Provides additional carrying capacity for Scavengers| 1 time at half durability | Carts require 2 Metal, 5 Wood, and 4 Hide to craft and take 10 work units to complete.|