title: Closing Out Your Turn
description: End of turn
authors: Justin Boucher

# Closing out your turn

After you have moved your player tokens, and performed the NPC spawning and movement phase, you can close out your turn. All of the steps below must be performed every turn, and must be performed in this order.

## Steps to Complete Your Turn

|Step|Action|
|:---|:---|
|**1**| [Reduce grain in Base Camp by one unit](#1-reducing-grain)|
|**2**| [Reduce wood stores by one unit for Base Camp fuel](#2-reduce-fuel-stores)|
|**3**| [Add new items to Base Camp stores](#3-4-settling-stores)|
|**4**| [Reduce items from Base Camp stores](#3-4-settling-stores)|
|**5**| Turn has been completed.

### 1. Reducing Grain

Grain reduction is performed to reduce the amount of available food and water resources. The amount of grain to reduce is the same if a player has 5 available player tokens or 1 token remaining, unless otherwise specified in the scenario. If there are no grain resources available to reduce, then the player must reduce each tokens health by one unit. If the player's token has reached a health of 0 they are downed in their current location and can only be revived by a healer.

!!! note
    Armor can't save you from starving to death. Reduction of health is performed on the token's health regardless of how much armor durability they have.

### 2. Reduce Fuel Stores

Player's need to be able to cook at Base Camp and keep their forges hot. Reducing available wood stores by one unit at the end of each turn to keep these services running. If there are no wood resources available, the Tinkerer can not reduce reduce their work timer until there are sufficient resources available.

### 3-4. Settling Stores

As the process is virtually the same for both these operations, they will be listed together below:

* If scavenger have returned items from their cart or resource queue at base camp, count up and add the required number of items for each resource.

* Reduce amount of resource stores for any craftable item that has been completed this round. Example: The player's Tinkerer token has completed building a wooden barrier. Player will remove 1 metal, 4 wood, and 1 leather from their Base Camp stores.

!!! question "Oops! I forgot how to count"
    What happens if you don't have the resources required to complete an item? This is a possibility during combat where downed player tokens have the remaining required resources in their queue. No problem. Just move the work timer back to one unit and try again next turn.